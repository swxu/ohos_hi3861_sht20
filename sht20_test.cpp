#include "DFRobot_SHT20.h"

#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"

extern "C" {
#include "hi_io.h"
#include "hi_i2c.h"
#include "hi_gpio.h"
}

static void ShtTask(void *arg)
{
    (void)arg;

    DFRobot_SHT20 sht20(HI_I2C_IDX_0);

    sht20.initSHT20();
    sht20.checkSHT20();

    while (1) {
        float humd = sht20.readHumidity();
        float temp = sht20.readTemperature();

        printf("humd: %.3f, temp: %.3f\n", humd, temp);
        osDelay(100);
    }
}

static void Sht30TestEntry(void)
{
    osThreadAttr_t attr = {0};

    hi_gpio_init();
    hi_io_set_func(HI_IO_NAME_GPIO_13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
    hi_io_set_func(HI_IO_NAME_GPIO_14, HI_IO_FUNC_GPIO_14_I2C0_SCL);
    hi_i2c_init(HI_I2C_IDX_0, 400 * 1000);

    attr.name = "ShtTask";
    attr.stack_size = 8192;
    attr.priority = osPriorityNormal;

    if (osThreadNew((osThreadFunc_t)ShtTask, NULL, &attr) == NULL) {
        printf("%s: create thread failed!\n", __func__);
    }
}
APP_FEATURE_INIT(Sht30TestEntry);
