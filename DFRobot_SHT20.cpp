/*!
 * @file  DFRobot_SHT20.cpp
 * @brief  Define the basic structure of class DFRobot_SHT20
 * @details  Drive SHT20 through this library to get temp and humidity
 * @copyright  Copyright (c) 2010 DFRobot Co.Ltd (http://www.dfrobot.com)
 * @license  The MIT License (MIT)
 * @author  [Zhangjiawei](jiawei.zhang@dfrobot.com)
 * @maintainer  [qsjhyy](yihuan.huang@dfrobot.com)
 * @version  V1.0
 * @date  2021-12-03
 * @url  https://github.com/DFRobot/DFRobot_SHT20
 */
#include "DFRobot_SHT20.h"

#include <math.h>
#include <stdio.h>
#include <string.h>

extern "C" {
#include "hi_i2c.h"
#include "hi_time.h"
}

static void delay(int ms)
{
  hi_udelay(ms * 1000);
}

#ifdef USE_ARDUINO
DFRobot_SHT20::DFRobot_SHT20(TwoWire *pWire, uint8_t sht20Addr)
{
  _addr = sht20Addr;
  _pWire = pWire;
}
#else
DFRobot_SHT20::DFRobot_SHT20(uint8_t idx, uint8_t sht20Addr)
{
  _addr = (sht20Addr << 1); // hi_i2c_read/write use 1bit moved address
  _i2cIdx = idx;
}
#endif

void DFRobot_SHT20::initSHT20()
{
#ifdef USE_ARDUINO
    _pWire->begin();
#endif
}

float DFRobot_SHT20::readHumidity(void)
{
    uint16_t rawHumidity = readValue(TRIGGER_HUMD_MEASURE_NOHOLD);
    if(rawHumidity == ERROR_I2C_TIMEOUT || rawHumidity == ERROR_BAD_CRC){
        return(rawHumidity);
    }
    float tempRH = rawHumidity * (125.0 / 65536.0);
    float rh = tempRH - 6.0;
    return (rh);
}

float DFRobot_SHT20::readTemperature(void)
{
    uint16_t rawTemperature = readValue(TRIGGER_TEMP_MEASURE_NOHOLD);
    if(rawTemperature == ERROR_I2C_TIMEOUT || rawTemperature == ERROR_BAD_CRC){
        return(rawTemperature);
    }
    float tempTemperature = rawTemperature * (175.72 / 65536.0);
    float realTemperature = tempTemperature - 46.85;
    return (realTemperature);
}

void DFRobot_SHT20::checkSHT20(void)
{
    byte reg = readUserRegister();
    showReslut("End of battery: ", reg & USER_REGISTER_END_OF_BATTERY);
    showReslut("Heater enabled: ", reg & USER_REGISTER_HEATER_ENABLED);
    showReslut("Disable OTP reload: ", reg & USER_REGISTER_DISABLE_OTP_RELOAD);
}

void DFRobot_SHT20::setResolution(byte resolution)
{
    byte userRegister = readUserRegister();
    userRegister &= 0x7E;// B01111110;
    resolution &= 0x81;  // B10000001;
    userRegister |= resolution;
    writeUserRegister(userRegister);
}

byte DFRobot_SHT20::readUserRegister(void)
{
    byte userRegister;
#ifdef USE_ARDUINO
    _pWire->beginTransmission(_addr);
    _pWire->write(READ_USER_REG);
    _pWire->endTransmission();
    _pWire->requestFrom(_addr, (uint8_t)1);
    userRegister = _pWire->read();
    return (userRegister);
#else
    hi_i2c_data data = {0};

    userRegister = READ_USER_REG;
    data.send_buf = (hi_u8 *) &userRegister;
    data.send_len = 1;
    if (hi_i2c_write((hi_i2c_idx) _i2cIdx, _addr, &data) != HI_ERR_SUCCESS) {
        printf("%s: send READ_USER_REG to %X failed!\n", __func__, _addr);
    }

    userRegister = 0;
    data.receive_buf = (hi_u8 *) &userRegister;
    data.receive_len = 1;
    if (hi_i2c_read((hi_i2c_idx) _i2cIdx, _addr, &data) != HI_ERR_SUCCESS) {
        printf("%s: recv READ_USER_REG from %X failed!\n", __func__, _addr);
    }
#endif
    return userRegister;
}

void DFRobot_SHT20::writeUserRegister(byte val)
{
#ifdef USE_ARDUINO
    _pWire->beginTransmission(_addr);
    _pWire->write(WRITE_USER_REG);
    _pWire->write(val);
    _pWire->endTransmission();
#else
    hi_u8 sendBuffer[] = {WRITE_USER_REG, val};
    hi_i2c_data data = {0};
    data.send_buf = sendBuffer;
    data.send_len = 2;
    if (hi_i2c_write((hi_i2c_idx) _i2cIdx, _addr, &data) != HI_ERR_SUCCESS) {
        printf("%s: send 2B to %X failed!\n", __func__, _addr);
    }
#endif
}

void DFRobot_SHT20::showReslut(const char *prefix, int val)
{
#ifdef USE_ARDUINO
    Serial.print(prefix);
    if(val){
        Serial.println("yes");
    }else{
        Serial.println("no");
    }
#else
    printf("%s%s\n", prefix, val ? "yes" : "no");
#endif
}

byte DFRobot_SHT20::checkCRC(uint16_t message_from_sensor, uint8_t check_value_from_sensor)
{
    uint32_t remainder = (uint32_t)message_from_sensor << 8;
    remainder |= check_value_from_sensor;
    uint32_t divsor = (uint32_t)SHIFTED_DIVISOR;
    for(int i = 0 ; i < 16 ; i++){
        if(remainder & (uint32_t)1 << (23 - i)){
            remainder ^= divsor;
        }
        divsor >>= 1;
    }
    return (byte)remainder;
}

uint16_t DFRobot_SHT20::readValue(byte cmd)
{
#ifdef USE_ARDUINO
    _pWire->beginTransmission(_addr);
    _pWire->write(cmd);
    if(0 != _pWire->endTransmission()){   // Used Wire.endTransmission() to end a slave transmission started by beginTransmission() and arranged by write().
      return (ERROR_I2C_TIMEOUT);
    }

    byte toRead;
    byte counter;
    for(counter = 0, toRead = 0 ; counter < MAX_COUNTER && toRead != 3; counter++){
        delay(DELAY_INTERVAL);
        toRead = _pWire->requestFrom(_addr, (uint8_t)3);
    }
    if(counter == MAX_COUNTER){
        return (ERROR_I2C_TIMEOUT);
    }
#else
    hi_i2c_data data = {0};
    data.send_buf = &cmd;
    data.send_len = 1;
    if (hi_i2c_write((hi_i2c_idx) _i2cIdx, _addr, &data) != HI_ERR_SUCCESS) {
        printf("%s: send cmd %X to %X failed!\n", __func__, cmd, _addr);
    }

    delay(MAX_WAIT);
#endif

    byte msb, lsb, checksum;
#ifdef USE_ARDUINO
    msb = _pWire->read();
    lsb = _pWire->read();
    checksum = _pWire->read();
#else
    hi_u8 recvBuffer[3] = {0};
    data.receive_buf = recvBuffer;
    data.receive_len = 3;
    if (hi_i2c_read((hi_i2c_idx) _i2cIdx, _addr, &data) != HI_ERR_SUCCESS) {
        printf("%s: recv 3B from %X failed!\n", __func__, _addr);
    }
    msb = recvBuffer[0];
    lsb = recvBuffer[1];
    checksum = recvBuffer[2];
#endif

    uint16_t rawValue = ((uint16_t) msb << 8) | (uint16_t) lsb;
    if(checkCRC(rawValue, checksum) != 0){
        return (ERROR_BAD_CRC);
    }
    return rawValue & 0xFFFC;
}
